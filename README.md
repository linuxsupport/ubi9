# Red Hat UBI9

This repository mirrors Red Hat's UBI9 container image:

https://catalog.redhat.com/software/containers/ubi8/5c647760bed8bd28d0e38f9f

https://www.redhat.com/en/blog/introducing-red-hat-universal-base-image

## Using this image with CERN mirrors

In order to use the CERN repository mirrors, you'll have to modify the repofile
as follows:

```dockerfile
FROM gitlab-registry.cern.ch/linuxsupport/ubi9/ubi

RUN sed -i 's#://cdn-ubi.redhat.com/#://linuxsoft.cern.ch/cdn-ubi.redhat.com/#' /etc/yum.repos.d/ubi.repo
```
